from django.db import models
from django.db.models import ForeignKey
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.models import ClusterableModel
# Create your models here.
from django.utils import timezone

from django.conf import settings


class Task(ClusterableModel):
    title = models.CharField(max_length=100)
    slug = models.SlugField(unique=True, max_length=100)
    details = models.TextField(blank=True)
    publish_date = models.DateTimeField(default=timezone.now)
    parent_task = ParentalKey('Task', blank=True, on_delete=models.CASCADE)
    due_date = models.DateTimeField(blank=True)
    completed_date = models.DateTimeField(blank=True)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="task_owner",
        on_delete=models.CASCADE,
    )
    assignees = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="task_doer",
        on_delete=models.SET_NULL,
    )
    completed_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="task_completer",
        on_delete=models.SET_NULL,
    )
    STATUS_CHOICES = (
        ('todo', 'To-Do'),
        ('doing', 'Doing'),
        ('done', 'Done'),
        ('failed', 'Failed'),
    )
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='todo')


class TaskList(ClusterableModel):
    title = models.CharField(max_length=100)
    details = models.TextField(blank=True)
    publish_date = models.DateTimeField(default=timezone.now)
    tasks = ParentalManyToManyField(Task, blank=True)
