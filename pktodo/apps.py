from django.apps import AppConfig


class PktodoConfig(AppConfig):
    name = 'pktodo'
