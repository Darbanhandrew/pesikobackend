###########
# BUILDER #
###########

# pull official base image
FROM python:3.8.3-alpine as builder
# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update \
    && apk add g++ gcc  python3-dev libxml2-dev libxslt-dev postgresql-dev musl-dev libffi libffi-dev

RUN apk add zlib-dev jpeg-dev

# lint
RUN pip install --no-cache --upgrade pip
#RUN pip install flake8
COPY . .
#RUN flake8 --ignore=E501,F401 .

# install dependencies
COPY ./requirements.txt .
RUN pip wheel --no-deps --wheel-dir /usr/src/app/wheels -r requirements.txt
#########
# FINAL #
#########

# pull official base image
FROM python:3.8.3-alpine

# create directory for the app user
RUN mkdir -p /home/app

# create the app user
#RUN addgroup -S app && adduser -S app -G app

# create the appropriate directories
ENV HOME=/home/app
ENV APP_HOME=/home/app/web
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/staticfiles
RUN mkdir $APP_HOME/mediafiles
WORKDIR $APP_HOME

# install dependencies
RUN apk update && apk add libpq
RUN apk update \
    && apk add g++ gcc  python3-dev libxml2-dev libxslt-dev postgresql-dev musl-dev libffi libffi-dev

RUN apk add zlib-dev jpeg-dev
RUN pip install --no-cache --upgrade pip
COPY --from=builder /usr/src/app/wheels /wheels
COPY --from=builder /usr/src/app/requirements.txt .
RUN pip install /wheels/*

# copy entrypoint-prod.sh
COPY ./entrypoint.sh $APP_HOME

# copy project
COPY . $APP_HOME

# chown all the files to the app user
#RUN chown -R app:app $APP_HOME

# change to the app user
#USER app

# run entrypoint.prod.sh
ENTRYPOINT ["/home/app/web/entrypoint.sh"]
#FROM python:3.8.3-alpine
#
## set work directory
#WORKDIR /usr/src/app
#
## set environment variables
#ENV PYTHONDONTWRITEBYTECODE 1
#ENV PYTHONUNBUFFERED 1
#
## install psycopg2 dependencies
#RUN apk update \
#    && apk add g++ gcc  python3-dev libxml2-dev libxslt-dev postgresql-dev musl-dev libffi libffi-dev
#
#RUN apk add zlib-dev jpeg-dev
#
## install dependencies
#RUN pip install --upgrade pip
#COPY ./requirements.txt .
#RUN pip install -r requirements.txt
## copy entrypoint.sh
#COPY ./entrypoint.sh .
#
## copy project
#COPY . .
#
#ENTRYPOINT ["/usr/src/app/entrypoint.sh"]