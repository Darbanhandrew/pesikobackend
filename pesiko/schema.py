from __future__ import unicode_literals
import graphene
from graphene_django import DjangoObjectType
from .models import BlogPage, ConditionsPage

from django.db import models


class ArticleNode(DjangoObjectType):
    class Meta:
        model = BlogPage
        only_fields = ['id', 'title', 'date', 'intro', 'body']


class ConditionsNode(DjangoObjectType):
    class Meta:
        model = ConditionsPage
        only_fields = ['id', 'title', 'date', 'intro', 'body', 'form_url']


class Query(graphene.ObjectType):
    articles = graphene.List(ArticleNode)
    conditions = graphene.List(ConditionsNode)
    condition_by_id = graphene.Field(ConditionsNode, id=graphene.ID())

    @graphene.resolve_only_args
    def resolve_articles(self):
        return BlogPage.objects.live()

    @graphene.resolve_only_args
    def resolve_conditions(self):
        return ConditionsPage.objects.live()

    @graphene.resolve_only_args
    def resolve_condition_by_id(self, id):
        return ConditionsPage.objects.get(pk=id)


schema = graphene.Schema(query=Query)
