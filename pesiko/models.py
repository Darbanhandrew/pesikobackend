from __future__ import unicode_literals

from modelcluster.contrib.taggit import ClusterTaggableManager
from oscar.apps.catalogue.models import Product
from django.db import models

# Create your models here.
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel
from wagtail.search import index
from wagtail.documents.models import Document
from wagtail.images.models import Image
from django.conf import settings
from modelcluster.models import ClusterableModel
from modelcluster.fields import ParentalManyToManyField, ParentalKey
from wagtail.snippets.models import register_snippet
from django import forms

from profiles.models import TeacherProfile, StudentProfile
from taggit.models import TaggedItemBase, Tag as TaggitTag


class Homepage(Page):
    max_count = 1
    parent_page_types = [
        'wagtailcore.Page'
    ]
    subpage_types =[
        'pesiko.FormListPage',
        'pesiko.BlogIndexPage',
        'pesiko.ConditionListPage',
        'pesiko.PodcastListPage',
        'pesiko.CourseListPage'
    ]
    intro = RichTextField(blank=True)
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]


class BlogIndexPage(Page):
    intro = RichTextField(blank=True)
    max_count = 1
    subpage_types = [
        'pesiko.BlogPage',  # appname.ModelName
    ]
    parent_page_types = [
        'pesiko.Homepage'  # appname.ModelName
    ]
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]


class FormListPage(Page):
    intro = RichTextField(blank=True)
    max_count = 1
    subpage_types = [
        'pesiko.Form',  # appname.ModelName
    ]
    parent_page_types = [
        'pesiko.Homepage'  # appname.ModelName
    ]
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]


class ConditionListPage(Page):
    intro = RichTextField(blank=True)
    max_count = 1
    subpage_types = [
        'pesiko.Condition',  # appname.ModelName
    ]
    parent_page_types = [
        'pesiko.Homepage'  # appname.ModelName
    ]
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]


class PodcastListPage(Page):
    intro = RichTextField(blank=True)
    max_count = 1
    subpage_types = [
        'pesiko.Podcast',  # appname.ModelName
    ]
    parent_page_types = [
        'pesiko.Homepage'  # appname.ModelName
    ]
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]


class CourseListPage(Page):
    intro = RichTextField(blank=True)
    max_count = 1
    subpage_types = [
        'pesiko.Course',  # appname.ModelName
    ]
    parent_page_types = [
        'pesiko.Homepage'  # appname.ModelName
    ]
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]


@register_snippet
class BlogCategory(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, max_length=80)

    panels = [
        FieldPanel('name'),
        FieldPanel('slug'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey('BlogPage', related_name='post_tags')


@register_snippet
class Tag(TaggitTag):
    class Meta:
        proxy = True


class BlogPage(Page, ClusterableModel):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    categories = ParentalManyToManyField('BlogCategory', blank=True)
    tags = ClusterTaggableManager(through='BlogPageTag', blank=True)
    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]
    subpage_types = []
    parent_page_types = [
        'pesiko.BlogIndexPage'  # appname.ModelName
    ]
    content_panels = Page.content_panels + [
        FieldPanel('slug'),
        FieldPanel('date'),
        FieldPanel('intro'),
        FieldPanel('body', classname="full"),
        FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        FieldPanel('tags'),
    ]


class Condition(BlogPage, ClusterableModel):
    form = ParentalManyToManyField('Form', blank=True)
    parent_page_types = [
        'pesiko.ConditionListPage'  # appname.ModelName
    ]
    subpage_types = []
    content_panels = BlogPage.content_panels + [
        FieldPanel('form', widget=forms.CheckboxSelectMultiple)
    ]


class Course(BlogPage, ClusterableModel):
    teacher = models.ForeignKey(
        TeacherProfile,
        blank=True,
        null=True,
        related_name="related_teacher",
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(Product, blank=True, null=True, on_delete=models.CASCADE,
                                related_name='related_product')
    trailer_video_url = models.URLField(default='')
    enrolled_students = ParentalManyToManyField(
        StudentProfile,
        through='EnrolledStudent',
        through_fields=('course', 'student_profile'),
    )
    content_panels = BlogPage.content_panels + [
        FieldPanel('teacher'),
        FieldPanel('product'),
        FieldPanel('enrolled_students', 'Students List')
    ]
    parent_page_types = [
        "pesiko.CourseListPage"
    ]
    subpage_types = [
        "pesiko.Session"
    ]


class EnrolledStudent(ClusterableModel):
    student_profile = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    total_mark = models.IntegerField(default=0)
    STATUS_CHOICES = (
        ('enrolled', 'Enrolled'),
        ('passed', 'Passed'),
        ('failed', 'Failed'),
    )
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='enrolled')


class Session(Page, ClusterableModel):
    date = models.DateField("Session Date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    order = models.IntegerField(default=0, blank=True)
    parent_session = ParentalKey('Session', blank=True, on_delete=models.CASCADE)
    homework = ParentalManyToManyField('Homework', related_name='session_homework', blank=True
                                       )
    quiz = ParentalManyToManyField('Quiz', related_name='session_quiz', blank=True)
    video_url = models.URLField(default='')
    parent_page_types = [
        "pesiko.Course"
    ]
    subpage_types = [
        "pesiko.Session"
    ]


class Assessment(Page):
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    total_mark = models.IntegerField(default=0, blank=True)
    related_file = models.ForeignKey(Document, null=True, blank=True, on_delete=models.SET_NULL,
                                     related_name='related_file')
    parent_page_types = []
    subpage_types = []

class Homework(Assessment):
    due_date = models.DateField("Homework Due Date")


class Quiz(Assessment):
    due_date = models.DateField("Quiz Due Date")


class EnrolledStudentQuiz(models.Model):
    enrolled_student = models.ForeignKey(EnrolledStudent, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    total_mark = models.IntegerField(default=0)
    STATUS_CHOICES = (
        ('enrolled', 'Enrolled'),
        ('passed', 'Passed'),
        ('failed', 'Failed'),
    )
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='enrolled')


class EnrolledStudentHomework(models.Model):
    enrolled_student = models.ForeignKey(EnrolledStudent, on_delete=models.CASCADE)
    homework = models.ForeignKey(Homework, on_delete=models.CASCADE)
    total_mark = models.IntegerField(default=0)
    STATUS_CHOICES = (
        ('enrolled', 'Enrolled'),
        ('passed', 'Passed'),
        ('failed', 'Failed'),
    )
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='enrolled')


class EnrolledStudentSessions(models.Model):
    enrolled_student = models.ForeignKey(EnrolledStudent, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    total_mark = models.IntegerField(default=0)
    STATUS_CHOICES = (
        ('enrolled', 'Enrolled'),
        ('passed', 'Passed'),
        ('failed', 'Failed'),
    )
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='enrolled')


@register_snippet
class FormCategory(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, max_length=80)

    panels = [
        FieldPanel('name'),
        FieldPanel('slug'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Form Category"
        verbose_name_plural = "Form Categories"


class Form(Page, ClusterableModel):
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    date = models.DateField()
    categories = ParentalManyToManyField('FormCategory', blank=True)
    FORM_TYPES = (
        ('quiz', 'Quiz'),
        ('homework', 'Homework'),
        ('condition-test', 'Condition Test'),
        ('constultant-form', 'Consultant Form'),
    )
    form_types = models.CharField(max_length=20,
                                  choices=FORM_TYPES,
                                  default='condition-test')
    form_url = models.URLField(default='')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=True, null=True,
                                related_name='related_products')
    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        FieldPanel('body'),
        FieldPanel('date', 'Publish Date'),
        FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        FieldPanel('form_types', 'Type of Form'),
        FieldPanel('form_url', 'Url of the Form'),
        FieldPanel('product')
    ]
    parent_page_types = [
        "pesiko.FormListPage"
    ]
    subpage_types = []


class Episode(Page):
    podcast_url = models.URLField(default='')
    order = models.IntegerField()
    parent_page_types = [
        "pesiko.Podcast"
    ]
    subpage_types = [
        "pesiko.Episode"
    ]

class Podcast(BlogPage):
    number_of_episodes = models.IntegerField(default=1)
    episodes = ParentalManyToManyField(Episode, blank=True)
    product = models.ForeignKey(Product, blank=True, null=True, on_delete=models.CASCADE,
                                related_name='episode_product')
    parent_page_types = [
        "pesiko.PodcastListPage"
    ]
    subpage_types = [
        "pesiko.Episode"
    ]
