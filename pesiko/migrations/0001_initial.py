# Generated by Django 3.0.10 on 2020-10-05 21:50

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.contrib.taggit
import modelcluster.fields
import wagtail.core.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtaildocs', '0010_document_file_hash'),
        ('catalogue', '0018_auto_20191220_0920'),
        ('profiles', '0002_auto_20200906_1220'),
        ('taggit', '0003_taggeditem_add_unique_index'),
        ('wagtailcore', '0052_pagelogentry'),
    ]

    operations = [
        migrations.CreateModel(
            name='Assessment',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', models.CharField(max_length=250)),
                ('body', wagtail.core.fields.RichTextField(blank=True)),
                ('total_mark', models.IntegerField(blank=True, default=0)),
                ('related_file', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='related_file', to='wagtaildocs.Document')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='BlogCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(max_length=80, unique=True)),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='BlogIndexPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', wagtail.core.fields.RichTextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='BlogPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('date', models.DateField(verbose_name='Post date')),
                ('intro', models.CharField(max_length=250)),
                ('body', wagtail.core.fields.RichTextField(blank=True)),
                ('categories', modelcluster.fields.ParentalManyToManyField(blank=True, to='pesiko.BlogCategory')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page', models.Model),
        ),
        migrations.CreateModel(
            name='ConditionListPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', wagtail.core.fields.RichTextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='CourseListPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', wagtail.core.fields.RichTextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='EnrolledStudent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_mark', models.IntegerField(default=0)),
                ('status', models.CharField(choices=[('enrolled', 'Enrolled'), ('passed', 'Passed'), ('failed', 'Failed')], default='enrolled', max_length=10)),
                ('student_profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='profiles.StudentProfile')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Episode',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('podcast_url', models.URLField(default='')),
                ('order', models.IntegerField()),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='FormCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(max_length=80, unique=True)),
            ],
            options={
                'verbose_name': 'Form Category',
                'verbose_name_plural': 'Form Categories',
            },
        ),
        migrations.CreateModel(
            name='FormListPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', wagtail.core.fields.RichTextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='Homepage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', wagtail.core.fields.RichTextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='PodcastListPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', wagtail.core.fields.RichTextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('taggit.tag',),
        ),
        migrations.CreateModel(
            name='Course',
            fields=[
                ('blogpage_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='pesiko.BlogPage')),
                ('trailer_video_url', models.URLField(default='')),
            ],
            options={
                'abstract': False,
            },
            bases=('pesiko.blogpage', models.Model),
        ),
        migrations.CreateModel(
            name='Homework',
            fields=[
                ('assessment_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='pesiko.Assessment')),
                ('due_date', models.DateField(verbose_name='Homework Due Date')),
            ],
            options={
                'abstract': False,
            },
            bases=('pesiko.assessment',),
        ),
        migrations.CreateModel(
            name='Quiz',
            fields=[
                ('assessment_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='pesiko.Assessment')),
                ('due_date', models.DateField(verbose_name='Quiz Due Date')),
            ],
            options={
                'abstract': False,
            },
            bases=('pesiko.assessment',),
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('date', models.DateField(verbose_name='Session Date')),
                ('intro', models.CharField(max_length=250)),
                ('body', wagtail.core.fields.RichTextField(blank=True)),
                ('order', models.IntegerField(blank=True, default=0)),
                ('video_url', models.URLField(default='')),
                ('parent_session', modelcluster.fields.ParentalKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='pesiko.Session')),
                ('homework', modelcluster.fields.ParentalManyToManyField(blank=True, related_name='session_homework', to='pesiko.Homework')),
                ('quiz', modelcluster.fields.ParentalManyToManyField(blank=True, related_name='session_quiz', to='pesiko.Quiz')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page', models.Model),
        ),
        migrations.CreateModel(
            name='Form',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', models.CharField(max_length=250)),
                ('body', wagtail.core.fields.RichTextField(blank=True)),
                ('date', models.DateField()),
                ('form_types', models.CharField(choices=[('quiz', 'Quiz'), ('homework', 'Homework'), ('condition-test', 'Condition Test'), ('constultant-form', 'Consultant Form')], default='condition-test', max_length=20)),
                ('form_url', models.URLField(default='')),
                ('categories', modelcluster.fields.ParentalManyToManyField(blank=True, to='pesiko.FormCategory')),
                ('product', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_products', to='catalogue.Product')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page', models.Model),
        ),
        migrations.CreateModel(
            name='EnrolledStudentSessions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_mark', models.IntegerField(default=0)),
                ('status', models.CharField(choices=[('enrolled', 'Enrolled'), ('passed', 'Passed'), ('failed', 'Failed')], default='enrolled', max_length=10)),
                ('enrolled_student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pesiko.EnrolledStudent')),
                ('session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pesiko.Session')),
            ],
        ),
        migrations.CreateModel(
            name='BlogPageTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content_object', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='post_tags', to='pesiko.BlogPage')),
                ('tag', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='pesiko_blogpagetag_items', to='taggit.Tag')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='blogpage',
            name='tags',
            field=modelcluster.contrib.taggit.ClusterTaggableManager(blank=True, help_text='A comma-separated list of tags.', through='pesiko.BlogPageTag', to='taggit.Tag', verbose_name='Tags'),
        ),
        migrations.CreateModel(
            name='Podcast',
            fields=[
                ('blogpage_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='pesiko.BlogPage')),
                ('number_of_episodes', models.IntegerField(default=1)),
                ('episodes', modelcluster.fields.ParentalManyToManyField(blank=True, to='pesiko.Episode')),
                ('product', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='episode_product', to='catalogue.Product')),
            ],
            options={
                'abstract': False,
            },
            bases=('pesiko.blogpage',),
        ),
        migrations.CreateModel(
            name='EnrolledStudentQuiz',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_mark', models.IntegerField(default=0)),
                ('status', models.CharField(choices=[('enrolled', 'Enrolled'), ('passed', 'Passed'), ('failed', 'Failed')], default='enrolled', max_length=10)),
                ('enrolled_student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pesiko.EnrolledStudent')),
                ('quiz', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pesiko.Quiz')),
            ],
        ),
        migrations.CreateModel(
            name='EnrolledStudentHomework',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_mark', models.IntegerField(default=0)),
                ('status', models.CharField(choices=[('enrolled', 'Enrolled'), ('passed', 'Passed'), ('failed', 'Failed')], default='enrolled', max_length=10)),
                ('enrolled_student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pesiko.EnrolledStudent')),
                ('homework', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pesiko.Homework')),
            ],
        ),
        migrations.AddField(
            model_name='enrolledstudent',
            name='course',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pesiko.Course'),
        ),
        migrations.AddField(
            model_name='course',
            name='enrolled_students',
            field=modelcluster.fields.ParentalManyToManyField(through='pesiko.EnrolledStudent', to='profiles.StudentProfile'),
        ),
        migrations.AddField(
            model_name='course',
            name='product',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_product', to='catalogue.Product'),
        ),
        migrations.AddField(
            model_name='course',
            name='teacher',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_teacher', to='profiles.TeacherProfile'),
        ),
        migrations.CreateModel(
            name='Condition',
            fields=[
                ('blogpage_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='pesiko.BlogPage')),
                ('form', modelcluster.fields.ParentalManyToManyField(blank=True, to='pesiko.Form')),
            ],
            options={
                'abstract': False,
            },
            bases=('pesiko.blogpage', models.Model),
        ),
    ]
