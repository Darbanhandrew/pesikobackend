from django.contrib import admin

# Register your models here.
from profiles.models import StudentProfile, TeacherProfile, PsychiatristProfile


class StudentProfileAdmin(admin.ModelAdmin):
    pass


admin.site.register(StudentProfile, StudentProfileAdmin)


class TeacherProfileAdmin(admin.ModelAdmin):
    pass


admin.site.register(TeacherProfile, TeacherProfileAdmin)


class PyschiatristProfileAdmin(admin.ModelAdmin):
    pass


admin.site.register(PsychiatristProfile, PyschiatristProfileAdmin)
