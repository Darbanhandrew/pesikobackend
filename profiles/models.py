from django.db import models
from django.conf import settings


# Create your models here.
class StudentProfile(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="student_user",
        on_delete=models.CASCADE,
    )


class TeacherProfile(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="teacher_user",
        on_delete=models.CASCADE,
    )


class PsychiatristProfile(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="psychiatrist_user",
        on_delete=models.CASCADE,
    )
